#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 20 09:59:07 2022

@author: felix
"""
from bs4 import BeautifulSoup # parser de html
import requests # client http
import re # regexps

MAX_LOOP = 3

URL_REGEX = "(?<=[\"\'])((?:\w*)(?:\:\/\/)(?:\w*\.)+(?:\w*)(?:\?\w+\=\w+(?:\&\w+\=\w+)*)*)(?=[\"\'])"
BASE_URL = 'https://www.jeuxvideo.com/forums/0-51-0-1-0-1-0-blabla-18-25-ans.htm'


urls = [BASE_URL]

targets = [BASE_URL]

loops = 0
while loops < MAX_LOOP:
  new_urls = []
  for target in targets:
    #recuperation html
    
    html_result = requests.get(target).text

    #regexp pour urls dans html
    links_on_page = re.findall(URL_REGEX, html_result)
    for link in links_on_page:
      if not link in urls:
        new_urls.append(link)
        urls.append(link)
  
  targets = new_urls
    
  loops += 1


#html_result = requests.get(BASE_URL).text;

#urls = re.findall(URL_REGEX, html_result)

print('end of all loops. urls: ', urls)
""",
soup = BeautifulSoup(r.text, "lxml");
links = soup.find_all('a');
print(links);
"""
